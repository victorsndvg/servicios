Protocolo de Configuración Dinámico. DHCP
=========================================

La configuración automática de los equipos que se conectan a una red siempre ha sido un elemento importante configuración, sin embargo hoy en día adquiere un carácter fundamental, dada la gran **cantidad y diversidad de dispositivos que pueden llegar a conectarse**. Además
hay que tener en cuenta que cada vez son menos las situaciones en las que un dispositivo tiene utilidad sin estar conectado a la red.

Los objetivos de este tema son:

    * Conocer el funcionamiento del **protocolo de configuración dinámica de host** (en inglés *Dynamic Host Configuration Protocol*, o simplemente **DHCP**).
    * Opciones de configuración de los clientes.
    * Configuración del servicio.
    * Características avanzadas.

Todo se desarrolla en los siguientes apartados:

.. toctree::
   :maxdepth: 2

   funcionamiento
   configuracion
   comprobacion
   adicionales
