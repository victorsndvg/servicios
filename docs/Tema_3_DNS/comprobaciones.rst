Comprobando el funcionamiento
==============================

Podemos utilizar unos cuantos comandos, ademas de consultar los ficheros de LOG de los distintos sistemas operativos, para comunicarnos y probar el funcionamiento de nuestros servidores DNS o incluso los externos, como por ejemplo:

  * **Comando Nslookup**: Este comando permite realizar consultas sobre los nombres de dominios de determinadas máquinas dirigidas a un servidor DNS.
    También permite obtener información sobre el servidor o servidores de nombres activos en un dominio determinado. **Existe tanto en sistemas Windows como en Linux**.
  * **Comando DIG**: Este comando tambien permite realizar consultas a servidores DNS, incluso con mas opciones que nslookup, presentando además una información más completa en sus respuetas. Presente unicamente en sistemas Linux.
  * **Comando HOST**:  Presente unicamente en sistemas Linux.
  * **Protocolo WHOIS**: TCP basado en petición/respuesta que se utiliza para efectuar consultas en una base de datos que permite determinar el propietario de un nombre de dominio o una dirección IP en Internet. Por linea de comandos o vía web.

  .. raw:: html

      <p style="text-align: justify;"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Pdf-2127829.png/480px-Pdf-2127829.png" alt="Perfil" width="50" style="vertical-align: middle; float:left;"/> En el documento Herramientas de consulta DNS puedes encontrar ejemplos de como utilizar estos comandos. </br> </br>

  .. image:: img/consultas_DNS.pdf
      :width: 400 px
      :alt: Tutorial consultas a servidores DNS
      :align: center
