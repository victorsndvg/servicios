Componentes del protocolo HTTP
==============================

Hypertext Transfer Protocol o HTTP (protocolo de transferencia de hipertexto) es el protocolo usado en cada transacción de la World Wide Web. HTTP  define la sintaxis y la semántica que utilizan los elementos de software de la arquitectura web (clientes, servidores, proxies) para comunicarse. Es un protocolo orientado a transacciones y sigue el esquema petición-respuesta entre un cliente y un servidor. Al cliente que efectúa la petición (NAVEGADOR WEB) se lo conoce como "user agent" (agente del usuario). A la información transmitida se la llama recurso y se la identifica mediante una dirección.


    .. warning::

        Para poder configurar los servicios web correctamente, debemos conocer una serie de términos relacionados con http[s], Internet o las páginas web en general.

Entre otros, algunos de los elementos más destacados del protocolo HTTP son los siguientes:

    * Request/Response
    * Cabeceras HTTP
    * Códigos de respuesta HTTP
    * URL/URI
    * Cookies
    * Métodos HTTP (Comandos en Linux)
    * Caché
    * HTTP vs HTTPS
    * Tipos MIME
    * Estilos
    * Métodos de autenticación
    * Sesión HTTP
    * Virtual Host
    * Web Stack

.. raw:: html

        <!-- </br>
        <div style="text-align: justify; color: orange; background-color: #e0e0e0; border-radius: 25px; padding-top: 20px;padding-right: 30px;padding-bottom: 20px; padding-left: 30px;">
        <u><b>EJERCICIO 1</b></u></br>
        Accede al <b>Ejercicio 1 - Componentes HTTP</b> del aula virtual. Vamos a definir entre tod@s los elementos anteriores con nuestras propias palabras.
        </div>
        </br> -->
