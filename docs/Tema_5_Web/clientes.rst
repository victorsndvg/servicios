Clientes HTTP. navegadores
==============================

.. raw:: html

    <p>
      La <b>definición</b> más simple de lo que es un navegador podría ser <sup id="fnref:note1"><a class="footnote-ref" href="#fn:note1" role="doc-noteref">1</a></sup>:</br>
      Software, aplicación o programa que permite el acceso a la Web, interpretando la información de distintos tipos de archivos y sitios web para que estos puedan ser vistos.
    </p>

Seguramente, uno de los programas que más utilices a diario en tu pc, móvil, etc...será un navegaor. Actualmente tenemos muchas versiones distintas de este tipo de aplicaciones:

    **WINDOWS/LINUX/MAC**

        * Ms Internet Explorer/Edge
        * Mozilla Firefox
        * Google Chrome/Chromium
        * Safari
        * Opera
        * Konqueror

.. raw:: html

    <p>
      Al ser una puerta de acceso a la web, utlizado por cualquier perfil de usuari@, puede ser un agujero de seguridad importante en nuestro entorno, es por esto que existen algunas buenas prácticas
      en relacion con su configuración y uso <sup id="fnref:note2"><a class="footnote-ref" href="#fn:note2" role="doc-noteref">2</a></sup>
    </p>

¿Cuál es el mejor navegador?¿Cuál es más seguro?Puedes encontrar opiniones para todos los gustos. Incluso puedes acudir a la web para ver qué navegadores son los más utilizados acutalmente:

.. raw:: html

    <div id="browser-ww-monthly-201912-202111" width="600" height="400" style="width:600px; height: 400px; margin-left:auto; margin-right:auto;"></div><!-- You may change the values of width and height above to resize the chart --><p>Source: <a href="https://gs.statcounter.com/browser-market-share/desktop-tablet-console/worldwide/#monthly-201912-202111-bar">StatCounter Global Stats - Browser Market Share</a></p><script type="text/javascript" src="https://www.statcounter.com/js/fusioncharts.js"></script><script type="text/javascript" src="https://gs.statcounter.com/chart.php?browser-ww-monthly-201912-202111&chartWidth=600"></script>

La mayoría de los navegadores actuales más utilizados incorporan muchísimas herramientas para poder analizar y comprender lo que sucede cuando navegamos por la red.

.. tip::
   Tanto si somos programadores como administradores, puede resultar bastante útil conocer algunas de estas utilidades:

   1. Para probar código CSS/HTML 'directamente'
   2. Para analizar las peticiones que se realizan a nuestros servidores
   3. Para ver la cantidad de información que se transmite en cada petición
   4. etc...


.. raw:: html

   </br>
   <div class="footnotes">
       <hr />
       <ol>
           <li class="footnote" id="fn:note1">
               <p>
                   <b>Fuente:</b> <a href="https://es.wikipedia.org/wiki/Navegador_web" target="_blank">https://es.wikipedia.org/wiki/Navegador_web</a>
                   <a class="footnote-backref" rev="footnote" href="#fnref:note1">&#8617;</a>
               </p>
           </li>
           <li class="footnote" id="fn:note2">
               <p>
                   <b>Ejemplo:</b> En la web del <a href="https://www.incibe.es/" target="_blank">INCIBE(Instituto Nacional de Ciberseguridad)</a>
                   puedes encontrar un <a href="https://www.incibe.es/extfrontinteco/img/File/intecocert/Proteccion/securizacion_de_navegadores.pdf" target="_blank"> manual para configurar tu navegador</a>, a nivel de usuari@<a class="footnote-backref" rev="footnote" href="#fnref:note2">&#8617;</a>
               </p>
           </li>
       </ol>
   </div>
